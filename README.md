Discussion 8
------------

- Programming a Quiz
    - At first figure out types of questions (e.g. multiple choice, true false and so on)
    - As a second step, if questions have common elements, capture common elements in an abstract class
    - Have concrete questions classes inherit from abstract class, if required override methods
    - Make use of super: initialize variables from parent class
    - Class variables (static ones): not use these in this case
    - Instance variables: use it to check if reply correct, count 1 if correct, count 0 if not correct
    - Make use of interfaces (e.g. ActionListener) to respond to user events (e.g. clicks)
- List of classes
    - QuestionDialog.java (represent question frame)
    - Question.java (abstract class to represent a question)
    - MultipleChoiceQuestion.java (represent multiple choice question)
    - TrueFalseQuestion.java (represent true false question)
    - Quiz.java (run a quiz and count correct/not correct replies)
- Implementation
    - implementation can be found [HERE](https://gitlab.com/nikolapav1985/java.peerg)
